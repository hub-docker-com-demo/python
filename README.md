# python

* https://hub.docker.com/_/python/
* https://pkgs.alpinelinux.org/package/v3.3/main/x86/python
* https://pkgs.alpinelinux.org/package/v3.3/main/x86/py-pip
* https://pkgs.alpinelinux.org/package/edge/main/x86/py2-pip

# Installing Packages
* [*Installing Packages*](https://packaging.python.org/tutorials/installing-packages/)
* [python-packages-demo](https://gitlab.com/python-packages-demo/python-packages-demo)

## See also
* [alpinelinux-packages-demo/python3](https://gitlab.com/alpinelinux-packages-demo/python3)
* [alpinelinux-packages-demo/python2](https://gitlab.com/alpinelinux-packages-demo/python2)

# Docker images
* [*Using Alpine can make Python Docker builds 50× slower*
  ](https://pythonspeed.com/articles/alpine-docker-python/)
  2020-01..2021-05 Itamar Turner-Trauring
